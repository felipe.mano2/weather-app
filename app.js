import path from 'path'
import express from 'express'
import routes from './src/routes/routes.js'
import exphbs from 'express-handlebars'


const app = express()
const port = 3000
const __dirname = path.resolve()

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(express.static('public'))
app.set('views', path.join(__dirname, 'src/views/pages'))
app.set('view engine', 'hbs')
app.engine('hbs', exphbs({extname: 'hbs', defaultLayout: 'layout', layoutsDir: __dirname + '/src/views/components'}))

routes(app)

app.listen(process.env.PORT || port, () => {
    console.log(`Server running on port ${port}`);
})


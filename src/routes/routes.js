import express from 'express'
import path from 'path'
import fs from 'fs'
import homeRoutes from './v1/homeRoutes.js'
import weatherRoutes from './v1/weatherRoutes.js'

export default (app) => {

    const v1Route = express.Router()

    homeRoutes(v1Route)
    weatherRoutes(v1Route)

    app.use('', v1Route)

}
import weatherControllers from "../../controllers/weatherControllers.js"


export default (v1Route) => {

    v1Route.route('/weather')

        .get(weatherControllers.getWeather)
        .post(weatherControllers.postWeather)

}
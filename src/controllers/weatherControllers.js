import axios from 'axios'


const getWeather = (req, res) => {
    res.render('weather')
}

const postWeather = (req, res) => {

    const reqLocation = req.body.location.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
    const weatherAPI = `https://api.openweathermap.org/data/2.5/weather?q=${reqLocation}&units=metric&APPID=c0265677a72eed43fb700323908200f1`
    const weatherImages = {
        "Clear": 'https://img.icons8.com/plasticine/100/000000/sun.png',
        "Clouds": 'https://img.icons8.com/windows/100/000000/cloud.png',
        "Rain": 'https://img.icons8.com/cotton/64/000000/rain--v3.png'
    }

    axios.get(weatherAPI)
        .then((response) => {

            const data = response.data

            req.body = {
                location: data.name,
                coord: {
                    lon: data.coord.lon,
                    lat: data.coord.lat
                },
                temps: {
                    now: data.main.temp,
                },
                mainWeather: data.weather[0].main,
                country: data.sys.country,
                weatherImages
            }


            const dailyAPI = `https://api.openweathermap.org/data/2.5/onecall?units=metric&lat=${req.body.coord.lat}&lon=${req.body.coord.lon}&exclude=minutely&appid=c0265677a72eed43fb700323908200f1`

            axios.get(dailyAPI)
                .then((response) => {


                    req.body = {
                        ...req.body,
                        daily: response.data.daily,
                        hourly: response.data.hourly
                    }

                    // console.log(req.body);
                    res.render('weather', req.body)
                })

        })
        .catch((error) => {

            res.render('weather', error)
            console.log(error);
        })
}

export default { getWeather, postWeather }
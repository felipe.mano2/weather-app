

const getHome = (req, res) => {
    res.render('home', {
        title: 'Home'
    })
}

export default { getHome }